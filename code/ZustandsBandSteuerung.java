enum Zustand {
    ENDSTATE,
    STARTSTATE,
    NACHLINKS,
    NACHRECHTS,
    STOPLINKS,
    STOPRECHTS
}

public class ZustandsBandSteuerung extends BandSteuerung {

    @Override
    public void steuere() {
        {
            Band b = getBand();
            b.open();

            Zustand aktuellerZustand = Zustand.STARTSTATE;
            boolean linkeLS = false;
            boolean rechteLS = false;
            boolean tasteR = false;
            boolean tasteL = false;
            final int timeout = 8000;

            while (!aktuellerZustand.equals(Zustand.ENDSTATE)) {
                linkeLS = b.getLinkeLS();
                rechteLS = b.getRechteLS();
                tasteR = b.getSchwarzeTaste();
                tasteL = b.getRoteTaste();

                if (tasteL && tasteR) {
                    aktuellerZustand = Zustand.ENDSTATE;
                } else if (tasteL) {
                    aktuellerZustand = Zustand.NACHLINKS;
                } else if (tasteR) {
                    aktuellerZustand = Zustand.NACHRECHTS;
                }

                switch (aktuellerZustand) {
                    case STARTSTATE:
                        b.setStop();
                        if (linkeLS) {
                            aktuellerZustand = Zustand.NACHRECHTS;
                        }
                        if (rechteLS) {
                            aktuellerZustand = Zustand.NACHLINKS;
                        }
                        break;
                    case NACHLINKS:
                        b.setNachLinks();
                        if (linkeLS) {
                            b.setStop();
                            aktuellerZustand = Zustand.STOPLINKS;
                        } else {
                            if (!b.warteAufLichtOderTastenAenderung(timeout)) {
                                aktuellerZustand = Zustand.STARTSTATE;
                            }
                        }
                        break;
                    case NACHRECHTS:
                        b.setNachRechts();
                        if (rechteLS) {
                            b.setStop();
                            aktuellerZustand = Zustand.STOPRECHTS;
                        } else {
                            if (!b.warteAufLichtOderTastenAenderung(timeout)) {
                                aktuellerZustand = Zustand.STARTSTATE;
                            }
                        }
                        break;
                    case STOPLINKS:
                        if (!linkeLS) {
                            aktuellerZustand = Zustand.NACHLINKS;
                        }
                        break;
                    case STOPRECHTS:
                        if (!rechteLS) {
                            aktuellerZustand = Zustand.NACHRECHTS;
                        }
                        break;
                    case ENDSTATE:
                        System.exit(0);
                        break;
                }
            }
        }
    }
}