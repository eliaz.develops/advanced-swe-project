public abstract class BandSteuerung {
    private Band band;

    public BandSteuerung(Band band) {
        setBand(band);
    }

    public BandSteuerung() {
        this(null);
    }

    public static void steuere
            (
                    Band band,
                    BandSteuerung steuerung
            ) {
        steuerung.setBand(band);
        steuerung.steuere();
    }

    public Band getBand() {
        return band;
    }

    public void setBand(Band band) {
        this.band = band;
    }

    public abstract void steuere();

}
