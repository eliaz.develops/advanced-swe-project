//	Anwendung von Bandsteuerungen des simulierten Bandes
public class SimuAnwendung {
    public static void main(String[] argument) {
        BandSteuerung bs = null;
        try {
            bs = (BandSteuerung) Class.forName(
                    "TestBandSteuerung"
                    //"AufgabeBandSteuerung"
                    //"LoesungBandSteuerung"
                    //"ScharrlachBandSteuerung"
            ).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        BandSteuerung.steuere
                (
                        SimuYoupiBand.newBaender(1)[0],
                        bs
                );
        System.exit(0);
        //	Damit die Graphik weggeht.
        //	Das muss man mal noch schöner machen.
    }

}

