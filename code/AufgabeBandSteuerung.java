public class AufgabeBandSteuerung extends BandSteuerung {
    //	Steuerung eines Kofferbands
    public void steuere() {
        Band b = getBand();
        b.open();

        //	Fügen Sie hier Ihre Lösung ein.
        // z.B.: b.setStop ();
        // if (b.getRoteTaste ()) ...
        // usw.

        b.close();
    }
}
