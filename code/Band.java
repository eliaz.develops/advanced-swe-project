public interface Band {
    void open();    //	eventuelle Band-Initialisierungen

    void close();    //	eventuelle Band-Abschaltungen

    void setStop();    //	stoppt das Band

    void setNachLinks();    //	bewegt das Band nach links

    void setNachRechts();    //	bewegt das Band nach rechts

    boolean getLinkeLS();
    //	= true, wenn Lichtschranke unterbrochen, = false sonst

    boolean getRechteLS();
    //	= true, wenn Lichtschranke unterbrochen, = false sonst

    boolean getSchwarzeTaste();
    //	= true, wenn schwarze Taste gedrückt ist, = false sonst

    boolean getRoteTaste();
    //	= true, wenn rote Taste gedrückt ist, = false sonst

    boolean warteAufLichtOderTastenAenderung(int timeout);
    //	Gibt true zurück, wenn an irgendeiner
    //	Lichtschranke oder Taste innerhalb von timeout Millisekunden
    //	eine Statusänderung eingetreten ist,
    //	oder gibt nach timeout Millisekunden false zurück.
    //	Falls timeout = 0, wird unendlich lang gewartet.
    //	Die aufrufende Task wird bis zum Eintreten der genannten
    //	Ereignisse blockiert.
}
