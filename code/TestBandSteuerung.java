public class TestBandSteuerung extends BandSteuerung {
    public TestBandSteuerung(Band band) {
        super(band);
    }

    public TestBandSteuerung() {
        super();
    }

    public void steuere() {
        Band b = getBand();
        b.open();
        System.out.println("Bandbetrieb wurde gestartet.");
        System.out.flush();
        try {
            Thread.sleep(1000);
            b.setNachLinks();
            Thread.sleep(3000);
            b.setStop();
            Thread.sleep(1000);
            b.setNachRechts();
            Thread.sleep(5000);
            b.setStop();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Bandbetrieb wurde eingestellt.");
        System.out.flush();
        if (b instanceof SimuYoupiBand) {
            System.exit(0);
            //	Damit die Graphik weggeht.
            //	Das muss mal schöner gemacht werden.
        }
        b.close();
    }

}
