import javax.swing.*;
import java.awt.*;

//	Simuliertes YoupiBand mit Totmannschaltung
public class TotmannSimuYoupiBand extends SimuYoupiBand implements Band {
    private static long zeitintervall = 500;    // ms
    private volatile Intervall intervall = new Intervall();

    {
        intervall.start();
    }

    public TotmannSimuYoupiBand(String name) {
        super(name);
    }

    public static Band[] newBaender(int anzahl) {
        String name = "Simuliertes YoupiBand mit Totmannschaltung";
        JFrame f = new JFrame(name);
        f.getContentPane().setLayout(new GridLayout(0, 1));

        Band[] k = new Band[anzahl];
        for (int i = 0; i < anzahl; i++) {
            k[i] = new TotmannSimuYoupiBand(name);
            f.getContentPane().add((SimuYoupiBand) k[i]);
        }
        f.pack();
        for (int i = 0; i < anzahl; i++) {
            ((SimuYoupiBand) k[i]).bandthread.start();
        }

        f.setVisible(true);

        return k;
    }

    public static void main(String[] arg) {
        Band k = TotmannSimuYoupiBand.newBaender(1)[0];
    }

    private void totKnopf() {
        if (!intervall.isAlive()) {
            intervall = new Intervall();
            intervall.start();
        } else {
            synchronized (intervall) {
                intervall.setTotFalse();
                intervall.notify();
            }
        }
    }

    public void open() {
    }

    public void close() {
        setStop();
    }

    public void setStop()    //	stoppt das Band
    {
        synchronized (intervall) {
            intervall.notify();
        }
        super.setStop();
    }

    public void setNachLinks()    //	bewegt das Band nach links
    {
        totKnopf();
        super.setNachLinks();
    }

    public void setNachRechts()    //	bewegt das Band nach rechts
    {
        totKnopf();
        super.setNachRechts();
    }

    private class Intervall extends Thread {
        private boolean tot = false;

        public void setTotFalse() {
            tot = false;
        }

        public void run() {
            synchronized (this) {
                tot = false;
                while (!tot) {
                    tot = true;
                    try {
                        wait(zeitintervall);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                setStop();
            }
        }
    }

}
