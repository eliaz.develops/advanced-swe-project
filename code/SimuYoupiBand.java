import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

//	Simuliertes YoupiBand
public class SimuYoupiBand extends JPanel implements Band {
    static Color kofferFarbe = new Color(0, 255, 255);
    protected Thread bandthread = new BandThread();
    long zeitschritt = 25;
    int bandschritt = 4; // war 2
    int a = 2;
    int b = 8;
    int g = 1;
    int h = 3;
    Warte warte;
    private String bezeichnung;
    private JButton linksKofferDrauf = new JButton("Aufsetzen");
    private JButton linksMitteKofferDrauf = new JButton("Aufsetzen");
    private JButton mitteKofferDrauf = new JButton("Aufsetzen");
    private JButton rechtsMitteKofferDrauf = new JButton("Aufsetzen");
    private JButton rechtsKofferDrauf = new JButton("Aufsetzen");
    private JButton linksKofferRunter = new JButton("Abnehmen");
    private JButton linksMitteKofferRunter = new JButton("Abnehmen");
    private JButton mitteKofferRunter = new JButton("Abnehmen");
    private JButton rechtsMitteKofferRunter = new JButton("Abnehmen");
    private JButton rechtsKofferRunter = new JButton("Abnehmen");
    private JButton linkeTaste = new JButton("Taste");
    private JButton rechteTaste = new JButton("Taste");
    private JButton doppelTaste = new JButton("Beide Tasten");
    //	Band:  xv ... xa ................ xe ... xh
    //				yo ... yu
    private int xv;
    private int xa;
    private int xe;
    private int xh;
    private int yo;
    private int yu;
    private int xp = 0;    //	Anfang gestrichelte Linie
    private int yt;    //	Lage der Linie
    private int yb;
    private int sl;    //	Strichlänge
    private double xvp = 0;    //	Prozentsätze
    private double xap = 10;
    private double xep = 100 - xap;
    private double xhp = 100;
    private double yop = 10;
    private double yup = 100 - yop;
    private double ytp = yop + 5;
    private double ybp = yup - 5;
    private double slp = 2;
    private double xls = xap + 3;    //	linke Lichtschranke
    private double xrs = xep - 3;    //	rechte Lichtschranke
    private int step = 0;    //	1; -1;
    private ArrayList<Koffer> koffer = new ArrayList<Koffer>();
    private Aufsetzen aufsetzen = new Aufsetzen();
    private Abnehmen abnehmen = new Abnehmen();
    private Dimension d;
    private BandJPanel band = new BandJPanel();
    private boolean pinLinkeLS;
    private boolean pinRechteLS;
    private boolean pinSchwarzeTaste;
    private boolean pinRoteTaste;
    private boolean linkeLS;
    private boolean rechteLS;

    {
        linksKofferDrauf.setForeground(Color.black);
    }

    {
        linksMitteKofferDrauf.setBackground(Color.lightGray);
        linksMitteKofferDrauf.setForeground(Color.black);
    }

    {
        mitteKofferDrauf.setBackground(Color.lightGray);
        mitteKofferDrauf.setForeground(Color.black);
    }

    {
        rechtsMitteKofferDrauf.setBackground(Color.lightGray);
        rechtsMitteKofferDrauf.setForeground(Color.black);
    }

    {
        rechtsKofferDrauf.setForeground(Color.black);
    }

    {
        linksKofferRunter.setForeground(Color.black);
    }

    {
        linksMitteKofferRunter.setBackground(Color.lightGray);
        linksMitteKofferRunter.setForeground(Color.black);
    }

    {
        mitteKofferRunter.setBackground(Color.lightGray);
        mitteKofferRunter.setForeground(Color.black);
    }

    {
        rechtsMitteKofferRunter.setBackground(Color.lightGray);
        rechtsMitteKofferRunter.setForeground(Color.black);
    }

    {
        rechtsKofferRunter.setForeground(Color.black);
    }

    //private JLabel	linkeTaste	= new JLabel ("Taste");
    //private JPanel	linkeTaste	= new JPanel ();
    {
        //linkeTaste.setBackground (Color.red);
        //linkeTaste.setForeground (Color.white);
        linkeTaste.setForeground(Color.red);
    }

    {
        //rechteTaste.setBackground (Color.black);
        //rechteTaste.setForeground (Color.white);
        rechteTaste.setForeground(Color.black);
    }

    {
        //doppelTaste.setBackground (Color.orange);
        //doppelTaste.setForeground (Color.white);
        doppelTaste.setForeground(Color.orange);
    }

    {
        band.setBackground(Color.blue);
        band.setForeground(Color.black);
    }

    {    //	Initialisator:
        GridBagLayout gb = new GridBagLayout();
        setLayout(gb);
        GridBagConstraints c = new GridBagConstraints();

        c.gridy = 0;
        for (int i = 0; i < a + b + 3; i++) {
            c.gridx = i;
            add(new Label("          "), c);
        }

        c.gridx = a + b + 2;
        for (int i = 1; i < g + h + 3; i++) {
            c.gridy = i;
            add(new Label("          "), c);
        }

        c.gridx = a;
        c.gridy = g;
        add(linksKofferDrauf, c);
        linksKofferDrauf.setName("" + c.gridx);
        linksKofferDrauf.addActionListener(aufsetzen);

        c.gridx = a;
        c.gridy = g + h;
        add(linksKofferRunter, c);
        linksKofferRunter.setName("" + c.gridx);
        linksKofferRunter.addActionListener(abnehmen);

        c.gridx = a + b / 4;
        c.gridy = g;
        add(linksMitteKofferDrauf, c);
        linksMitteKofferDrauf.setName("" + c.gridx);
        linksMitteKofferDrauf.addActionListener(aufsetzen);

        c.gridx = a + b / 4;
        c.gridy = g + h;
        add(linksMitteKofferRunter, c);
        linksMitteKofferRunter.setName("" + c.gridx);
        linksMitteKofferRunter.addActionListener(abnehmen);

        c.gridx = a + b / 2;
        c.gridy = g;
        add(mitteKofferDrauf, c);
        mitteKofferDrauf.setName("" + c.gridx);
        mitteKofferDrauf.addActionListener(aufsetzen);

        c.gridx = a + b / 2;
        c.gridy = g + h;
        add(mitteKofferRunter, c);
        mitteKofferRunter.setName("" + c.gridx);
        mitteKofferRunter.addActionListener(abnehmen);

        c.gridx = a + b;
        c.gridy = g;
        add(rechtsKofferDrauf, c);
        rechtsKofferDrauf.setName("" + c.gridx);
        rechtsKofferDrauf.addActionListener(aufsetzen);

        c.gridx = a + b / 4 * 3;
        c.gridy = g;
        add(rechtsMitteKofferDrauf, c);
        rechtsMitteKofferDrauf.setName("" + c.gridx);
        rechtsMitteKofferDrauf.addActionListener(aufsetzen);

        c.gridx = a + b / 4 * 3;
        c.gridy = g + h;
        add(rechtsMitteKofferRunter, c);
        rechtsMitteKofferRunter.setName("" + c.gridx);
        rechtsMitteKofferRunter.addActionListener(abnehmen);

        c.gridx = a + b;
        c.gridy = g + h;
        add(rechtsKofferRunter, c);
        rechtsKofferRunter.setName("" + c.gridx);
        rechtsKofferRunter.addActionListener(abnehmen);

        c.gridx = a;
        c.gridy = g + h + 1;
        add(linkeTaste, c);
	/*
	linkeTaste.addActionListener
		(
		new ActionListener ()
			{
			public void	actionPerformed (ActionEvent e)
				{
				//step = -bandschritt;
				pinRoteTaste = true;
				entprelle ();
				pinRoteTaste = false;
				}
			}
		);
	*/
        linkeTaste.addMouseListener
                (
                        new MouseAdapter() {
                            public void mousePressed(MouseEvent e) {
                                pinRoteTaste = true;
                                if (warte != null) warte.interrupt();
                            }

                            public void mouseReleased(MouseEvent e) {
                                pinRoteTaste = false;
                                if (warte != null) warte.interrupt();
                            }
                        }
                );

        c.gridx = a + b;
        c.gridy = g + h + 1;
        add(rechteTaste, c);
	/*
	rechteTaste.addActionListener
		(
		new ActionListener ()
			{
			public void	actionPerformed (ActionEvent e)
				{
				//step = bandschritt;
				pinSchwarzeTaste = true;
				entprelle ();
				pinSchwarzeTaste = false;
				}
			}
		);
	*/
        rechteTaste.addMouseListener
                (
                        new MouseAdapter() {
                            public void mousePressed(MouseEvent e) {
                                pinSchwarzeTaste = true;
                                if (warte != null) warte.interrupt();
                            }

                            public void mouseReleased(MouseEvent e) {
                                pinSchwarzeTaste = false;
                                if (warte != null) warte.interrupt();
                            }
                        }
                );


        c.gridx = (a + a + b) / 2;
        c.gridy = g + h + 1;
        add(doppelTaste, c);
	/*
	doppelTaste.addActionListener	//	Reagiert erst auf MouseRelease
		(
		new ActionListener ()
			{
			public void	actionPerformed (ActionEvent e)
				{
				//step = 0;
				pinRoteTaste = true;
				pinSchwarzeTaste = true;
				entprelle ();
				pinRoteTaste = false;
				pinSchwarzeTaste = false;
				}
			}
		);
	*/
        doppelTaste.addMouseListener
                (
                        new MouseAdapter() {
                            public void mousePressed(MouseEvent e) {
                                pinRoteTaste = true;
                                pinSchwarzeTaste = true;
                                if (warte != null) warte.interrupt();
                            }

                            public void mouseReleased(MouseEvent e) {
                                pinRoteTaste = false;
                                pinSchwarzeTaste = false;
                                if (warte != null) warte.interrupt();
                            }
                        }
                );

        c.gridx = a - 1;
        c.gridy = g + 1;
        c.gridwidth = b + 3;
        c.gridheight = h - 1;
        c.fill = GridBagConstraints.BOTH;
        add(band, c);

    }

    public SimuYoupiBand(String name) {
        setName(name);
    }

    public static Band[] newBaender(int anzahl) {
        String name = "Simuliertes YoupiBand";
        JFrame f = new JFrame(name);
        f.getContentPane().setLayout(new GridLayout(0, 1));

        Band[] k = new Band[anzahl];
        for (int i = 0; i < anzahl; i++) {
            k[i] = new SimuYoupiBand(name);
            f.getContentPane().add((SimuYoupiBand) k[i]);
        }
        f.pack();
        for (int i = 0; i < anzahl; i++) {
            ((SimuYoupiBand) k[i]).bandthread.start();
        }

        f.setVisible(true);

        return k;
    }

    public static void main(String[] arg) {
        Band k = SimuYoupiBand.newBaender(1)[0];
    }

    public String getBezeichnung() {
        return bezeichnung;
    }


    //	YoupiBand-Stuff:

    private void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    private int prozToPix(int dwh, double z) {
        return (int) Math.round(dwh * z / 100.0);
    }

    private double pixToProz(int dwh, int p) {
        return (double) p / (double) dwh * 100.0;
    }

    private void entprelle() {
        if (warte != null) warte.interrupt();
        try {
            Thread.sleep(20);
        } catch (InterruptedException e) {
        }
        if (warte != null) warte.interrupt();
    }

    public void open() {
    }

    public void close() {
        setStop();
    }

    public void setStop()    //	stoppt das Band
    {
        step = 0;
    }

    public void setNachLinks()    //	bewegt das Band nach links
    {
        step = -bandschritt;
    }

    public void setNachRechts()    //	bewegt das Band nach rechts
    {
        step = bandschritt;
    }

    public boolean getLinkeLS()
    //	= true, wenn Lichtschranke unterbrochen, = false sonst
    {
        return pinLinkeLS;
    }

    public boolean getLinkeLSOhneAktualisierung()
    //	= true, wenn Lichtschranke unterbrochen, = false sonst
    {
        return pinLinkeLS;
    }

    public boolean getRechteLS()
    //	= true, wenn Lichtschranke unterbrochen, = false sonst
    {
        return pinRechteLS;
    }

    public boolean getRechteLSOhneAktualisierung()
    //	= true, wenn Lichtschranke unterbrochen, = false sonst
    {
        return pinRechteLS;
    }

    public boolean getSchwarzeTaste()
    //	= true, wenn schwarze Taste gedrückt ist, = false sonst
    {
        return pinSchwarzeTaste;
    }

    public boolean getSchwarzeTasteOhneAktualisierung()
    //	= true, wenn schwarze Taste gedrückt ist, = false sonst
    {
        return pinSchwarzeTaste;
    }

    public boolean getRoteTaste()
    //	= true, wenn rote Taste gedrückt ist, = false sonst
    {
        return pinRoteTaste;
    }

    public boolean getRoteTasteOhneAktualisierung()
    //	= true, wenn rote Taste gedrückt ist, = false sonst
    {
        return pinRoteTaste;
    }

    public boolean warteAufLichtOderTastenAenderung(int timeout)
    //	Gibt true zurück, wenn an irgendeiner
    //	Lichtschranke oder Taste innerhalb von timeout Millisekunden
    //	eine Statusänderung eingetreten ist,
    //	oder gibt nach timeout Millisekunden false zurück.
    //	Falls timeout = 0, wird unendlich lang gewartet.
    {
        warte = new Warte(timeout);
        warte.start();
        try {
            warte.join();
        } catch (InterruptedException e) {
        }
        return warte.intr;
    }

    private class Koffer extends Rectangle {
        double pos;
        double w = 4;
        double h = 20;

        public Koffer(String pos) {
            this.pos = pixToProz(d.width, xa) + w * 0.75
                    + (pixToProz(d.width, xe - xa) - w * 1.5)
                    * (((double) Integer.parseInt(pos)) - a) / b;
            this.x = prozToPix(d.width, this.pos - w / 2);
            this.y = prozToPix(d.height, 50.0 - h / 2);
            this.width = prozToPix(d.width, w);
            this.height = prozToPix(d.height, h);
        }

        public boolean schneidet(ArrayList<Koffer> k) {
            int i = 0;
            while (i < k.size()) {
                if (intersects(k.get(i))) return true;
                i++;
            }
            return false;
        }

        public void male(Graphics g) {
            g.setColor(kofferFarbe);
            g.fillRect(this.x, this.y, this.width, this.height);
        }

    }

    private class Aufsetzen
            implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            Koffer k = new Koffer(((JButton) e.getSource()).getName());
            if (k.schneidet(koffer)) return;
            koffer.add(k);
            band.repaint();
        }
    }

    private class Abnehmen
            implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            int pos = prozToPix(d.width,
                    ((((double) Integer.parseInt(
                            ((JButton) e.getSource()).getName()
                    )) - 1) * 100) / (b + 2));
            int bereich = prozToPix(d.width, 5);
            int i = 0;
            Koffer k;
            while (i < koffer.size()) {
                k = koffer.get(i);
                if (k.x + k.width > pos - bereich && k.x < pos + bereich) {
                    koffer.remove(k);
                    i--;
                }
                i++;
            }
            band.repaint();
        }
    }

    private class BandJPanel
            extends JPanel {
        public void paint(Graphics g) {
            super.paint(g);

            d = getSize();
            xv = prozToPix(d.width, xvp);
            xa = prozToPix(d.width, xap);
            xe = prozToPix(d.width, xep);
            xh = prozToPix(d.width, xhp);
            yo = prozToPix(d.height, yop);
            yu = prozToPix(d.height, yup);
            yt = prozToPix(d.height, ytp);
            yb = prozToPix(d.height, ybp);
            sl = prozToPix(d.width, slp);

            g.setColor(Color.yellow);
            g.fillRoundRect(xa, yo, xe - xa, yu - yo, 5, 3);
            if (pinLinkeLS) g.setColor(Color.black);
            else g.setColor(Color.red);
            g.drawLine
                    (
                            prozToPix(d.width, xls),
                            prozToPix(d.height, 0),
                            prozToPix(d.width, xls),
                            prozToPix(d.height, 100)
                    );
            if (pinRechteLS) g.setColor(Color.black);
            else g.setColor(Color.red);
            g.drawLine
                    (
                            prozToPix(d.width, xrs),
                            prozToPix(d.height, 0),
                            prozToPix(d.width, xrs),
                            prozToPix(d.height, 100)
                    );

            g.setColor(Color.black);
            if (xp < xa) xp = prozToPix(d.width, xa + sl);
            int xx = xp - sl;
            int x2;
            while (xx >= xa) {
                x2 = xx - sl;
                if (x2 < xa) x2 = xa;
                g.drawLine(xx, yt, x2, yt);
                g.drawLine(xx, yb, x2, yb);
                xx = xx - 2 * sl;
            }
            xx = xp;
            while (xx < xe) {
                x2 = xx + sl;
                if (x2 > xe) x2 = xe;
                g.drawLine(xx, yt, x2, yt);
                g.drawLine(xx, yb, x2, yb);
                xx = xx + 2 * sl;
            }

            int i = 0;
            while (i < koffer.size()) {
                koffer.get(i).male(g);
                i++;
            }
        }    //	end paint
    }

    private class BandThread
            extends Thread {
        public void run() {
            while (true) {
                try {
                    Thread.sleep(zeitschritt);
                } catch (InterruptedException e) {
                }
                xp = xp + step;
                if (xp > xe) xp = xa;
                if (xp < xa) xp = xe; // neu

                int i = 0;
                Koffer k;
                linkeLS = false;
                rechteLS = false;
                while (i < koffer.size()) {
                    k = koffer.get(i);

                    if (k.x + k.width / 2 < xa) k.x = k.x - bandschritt;
                    else if (k.x + k.width / 2 > xe) k.x = k.x + bandschritt;
                    else k.x = k.x + step;

                    if (k.x + k.width < xv || k.x > xh) {
                        koffer.remove(k);
                    } else {
                        int rls = prozToPix(d.width, xls);
                        if (k.x <= rls && k.x + k.width >= rls) linkeLS = true;
                        rls = prozToPix(d.width, xrs);
                        if (k.x <= rls && k.x + k.width >= rls) rechteLS = true;
                        i++;
                    }
                }
                if (pinLinkeLS != linkeLS || pinRechteLS != rechteLS) {
                    pinLinkeLS = linkeLS;
                    pinRechteLS = rechteLS;
                    if (warte != null) warte.interrupt();
                }
                band.repaint();
            }
        }
    }

    class Warte extends Thread {
        long timeout;
        boolean intr = false;

        public Warte(long timeout) {
            this.timeout = timeout;
        }

        public void run() {
            boolean forever = false;
            if (timeout == 0) {
                timeout = 100000;
                forever = true;
            }
            do {
                try {
                    Thread.sleep(timeout);
                } catch (InterruptedException e) {
                    intr = true;
                    forever = false;
                }
            } while (forever);
        }
    }

}
