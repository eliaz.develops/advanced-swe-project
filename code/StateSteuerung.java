public class StateSteuerung extends BandSteuerung {
    private Context context;
    private Band b;
    private boolean leftLS = false;
    private boolean rightLS = false;
    private boolean rightT = false;
    private boolean leftT = false;

    @Override
    public void steuere() {
        context = new Context(new Start());
        b = getBand();
        b.open();
        while (!context.state.getClass().getName().equals("End")) {
            if (rightT && leftT) {
                context.setState(new End());
                break;
            }
            if (rightT) {
                context.setState(new Right());
            }
            if (leftT) {
                context.setState(new Left());
            }
            leftLS = b.getLinkeLS();
            rightLS = b.getRechteLS();
            leftT = b.getRoteTaste();
            rightT = b.getSchwarzeTaste();
            context.request();
        }
        b.close();
    }

    interface State {
        void handle();
    }

    class Context {
        State state;

        Context(State s) {
            state = s;
        }

        void setState(State s) {
            state = s;
        }

        void request() {
            state.handle();
        }
    }

    class Start implements State {

        @Override
        public void handle() {
            b.setStop();
            if (leftLS) {
                context.setState(new Right());
            }
            if (rightLS) {
                context.setState(new Left());
            }
        }
    }

    class Right implements State {

        @Override
        public void handle() {
            b.setNachRechts();
            if (rightLS) {
                b.setStop();
                context.setState(new RightStop());
            } else {
                if (!b.warteAufLichtOderTastenAenderung(8000))
                    context.setState(new Start());
            }
        }
    }

    class Left implements State {

        @Override
        public void handle() {
            b.setNachLinks();
            if (leftLS) {
                b.setStop();
                context.setState(new LeftStop());
            } else {
                if (!b.warteAufLichtOderTastenAenderung(8000))
                    context.setState(new Start());
            }
        }
    }

    class LeftStop implements State {

        @Override
        public void handle() {
            if (!leftLS) {
                context.setState(new Right());
            }
        }
    }

    class RightStop implements State {

        @Override
        public void handle() {
            if (!rightLS) {
                context.setState(new Right());
            }
        }
    }

    class End implements State {

        @Override
        public void handle() {

        }
    }
}
